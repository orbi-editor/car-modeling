function [vsNew] = myCar(vsOld,control,tOld,dT)

t0=tOld;
tf=t0+dT;
[t,vs] = ode45(@(tt,vu)bicycleModelAuto3(tt,vu,control),[t0,tf],vsOld);

%Vx=vs(:,1);
%Vy=vs(:,2);
%r=vs(:,3);
%psi=vs(:,4);
%x0=vs(:,5);
%y0=vs(:,6);
%delta=vs(:,7);
%s0=vs(:,8);

vsNew=vs(end,:);

end