clear all;
clc;

max=60;

max=max*pi/180;

alfaF=0:max/500:max;
alfaR=0:max/500:max;

Ff=getFf(alfaF);
Fr=getFr(alfaR);

alfaF=alfaF./pi.*180;
alfaR=alfaR./pi.*180;

fig=figure(1);
    subplot(1,2,1);
        plot(alfaF,Ff);
        grid on;
        xlabel('alfa_f, grad');
        ylabel('F_f, N');
    subplot(1,2,2);
        plot(alfaR,Fr);
        grid on;
        xlabel('alfa_r, grad');
        ylabel('F_r, N');
saveas(fig,'F_alfa.png');
saveas(fig,'F_alfa.fig');