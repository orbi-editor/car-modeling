function Fr=getFr(alfaR)
%function for determinate friction force of tire
%none linear

Ar=1625000;
B=0.01;
C=8;
k=1;
Ar=k*Ar;

Fr=Ar*sin(B*atan(C*alfaR));

%{
Cr=130000;
Fr=Cr*alfaR;
%}
end