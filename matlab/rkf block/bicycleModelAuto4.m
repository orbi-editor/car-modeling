function vecdot=bicycleModelAuto4(t,vec,tempU)
%with 0.9
%const
m=2000;
a=1.2;
b=1.8;

Ksr=16;
I2=1800;
tau=0.008;
%vector input
Vx=vec(1);
Vy=vec(2);
r=vec(3);

psi=vec(4);
x0=vec(5);
y0=vec(6);

delta=vec(7);

s0=vec(8);
%dif eq
alfaF=-atan((Vy+a*r)/Vx)+delta;
alfaR=-atan((Vy-b*r)/Vx);

Fr=getFr(alfaR)*0.9;
Ff=getFf(alfaF)*0.9;

dotVx=1/m*(-Ff*sin(delta)+m*r*Vy);
dotVy=1/m*(Ff.*cos(delta)+Fr-m*r*Vx);
dotr=1/I2*(a*Ff*cos(delta)-b*Fr);

dotpsi=r;
dotx0=Vx*cos(psi)-Vy*sin(psi);
doty0=Vx*sin(psi)+Vy*cos(psi);

dotdelta=(tempU/Ksr-delta)/tau;

dots0=sqrt(dotx0^2+doty0^2);
%output
vecdot=[dotVx;dotVy;dotr;dotpsi;dotx0;doty0;dotdelta;dots0];
end