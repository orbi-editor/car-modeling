function [vecx,vecu]=lightPrepare(varVec,L,u0,uEnd)

u1=varVec(1);
u2=-varVec(2);
L1=L/2-varVec(3);
%L1=L/2;

tg1=pi/10;
tg2=pi/20;
tg3=pi/20;

vecx(1)=0;%vs();
vecx(2)=abs(u1-u0)/tg1;
vecx(3)=L1-abs(u1)/tg2;
vecx(4)=L1;
vecx(5)=L1+abs(u2)/tg2;
vecx(6)=L-abs(u2-uEnd)/tg3;
vecx(7)=L;

vecu(1)=u0;%vs();
vecu(2)=u1;
vecu(3)=u1;
vecu(4)=0;
vecu(5)=u2;
vecu(6)=u2;
vecu(7)=uEnd;

end