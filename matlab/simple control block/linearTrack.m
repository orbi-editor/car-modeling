function params=linearTrack()
%parameters of street
L=60;
h=3;
%physics parameters
psi_max=2*pi/180;
dudx=2*pi/20;%max du/dx=pi rad on 1 second
%parameters
params(1)=L;
params(2)=h;
params(3)=psi_max;
params(4)=dudx;
end