function u =lightUprav(x0,VECX,VECU)
%���� �������� ���� � ��������
%�������� ������������
len=length(VECX);
%[len,VECX,VECU]
n=0;
for i=1:len
    if x0>VECX(i)
        n=i;
    end
end
if n==0
    u=VECU(1);
elseif n>0 && n<len
    u=VECU(n)+(x0-VECX(n))*(VECU(n+1)-VECU(n))/(VECX(n+1)-VECX(n));
elseif n>=len
        u=VECU(len);
end
end