function Fr=getFr(alfaR)
%function for determinate friction force of tire
%none linear
Ar=1625000;
B=0.01;
C=8;

%Ar=Ar/2;

Fr=Ar*sin(B*atan(C*alfaR));
%Cr=130000;
%Fr=Cr*alfaR;
end