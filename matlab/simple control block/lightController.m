function [control,u_opt]=controller(uOld,vs0,vsTemp,vsEnd,u0,uEnd,dT,L)

if vsTemp(8)>=L
    control=uEnd;
    u_opt=uOld;
else
u1=0.31;
u2=0.3;

[e1_y0,e1_psi]=lightEPCost([u1;u1;0],vsTemp,vsEnd,u0,uEnd,dT,L);
[e2_y0,e2_psi]=lightEPCost([u2;u2;0],vsTemp,vsEnd,u0,uEnd,dT,L);

zn=(e1_y0-e2_y0);
if abs(zn)<0.001
    u_opt=uOld;
else
    u_opt=(u2*e1_y0-u1*e2_y0)/zn;%����������� ������ ��������
end

if(u_opt>=pi*99/180)
    u_opt=pi*99/180;
end

if(u_opt<=0)
    u_opt=0;
end

ugmax=720*dT*pi/180*10;
%{
if(u_opt-uOld>ugmax)
    u_opt=uOld+ugmax;
end

if(-u_opt+uOld<ugmax)
    u_opt=uOld-ugmax;
end
%}
%[ey0,epsi]=lightEPCost([u_opt;u_opt;0],vsTemp,vsEnd,u0,uEnd,dT,L)
%��������� ������� ������ ���������� (��� vs0)
[vecx,vecu]=lightPrepare([u_opt;u_opt;0],L,u0,uEnd);
control=lightUprav(vsTemp(8),vecx,vecu);
end

end