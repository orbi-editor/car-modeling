function Ff=getFf(alfaF)
%function for determinate friction force of tire
%none linear
Af=1250000;
B=0.01;
C=8;

%Af=Af/2;

Ff=Af*sin(B*atan(C*alfaF));
%Cf=100000;
%Ff=Cf*alfaF;
end