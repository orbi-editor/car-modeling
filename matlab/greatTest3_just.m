clear all;
addPath('rkf block');
addPath('simple control block');
clc;

L=60;%����� ������ (���� �� ����)
vs0=[20,0,0,0,0,0,0,0];%��������� ������ ��������
vsEnd=[20,0,0,0,L,3,0,L];%�������� �������� ������ ���������
vsTemp=vs0;%������� ������ ���������
u0=0;%��������� ����������. �������������� �� ����� ������
uEnd=0;%�������� ����������. �������������� �� ����� ������
dT=0.003;%�������� �������������� ������� ������ � ����� ��������

T=0.003;%�������� ������������� ��� ��� ����� ��������� ����������

ttemp=0;%������������� �������
i=1;
hOld=0;
VyTemp=0;
while ttemp<3.5
    %tic;
    [control(i),h(i)]=lightController(hOld,vs0,vsTemp,vsEnd,u0,uEnd,dT,L);%������� ����������
    %toc;%����� ������� ���������� ������� ����������
    %x0(end)
    Vx(i)=vsTemp(1);
    Vy(i)=vsTemp(2);
    r(i)=vsTemp(3);
    psi(i)=vsTemp(4);
    x0(i)=vsTemp(5);
    y0(i)=vsTemp(6);
    s0(i)=vsTemp(8);
    Ay(i)=(Vy(i)-VyTemp)/T+r(i)*Vx(i);
    %Ay(i)=-r(i)*Vx(i);
    VyTemp=Vy(i);
    %s0(end)
    vsTemp=myCar(vsTemp,control(i),ttemp,T);%�������� ��������� �/�
    %vsTemp=filter1(vsTemp);%������ ��� ���������� ������
    ttemp=ttemp+T;
    hOld=h(i);
    i=i+1;
end

control=control./pi.*180;
h=h./pi.*180;
r=r./pi.*180;
psi=psi./pi.*180;
fig=figure(1);
    subplot(2,2,2);
        plot(x0,y0,'LineWidth',2);
        grid on;
        %set(gca, 'YTick',0:0.5:3);
        xlabel('x_0, m');
        ylabel('y_0, m');
    subplot(2,2,1);
        plot(s0,h,'--',s0,control,'-','LineWidth',2);
        grid on;
        %set(gca, 'YTick',-50:10:50);
        xlabel('s_0, m');
        ylabel('h, u, grad');
        legend('h','u',2);
    subplot(2,2,3);
        plot(s0,Ay,'LineWidth',2);
        grid on;
        set(gca, 'YTick',-5:1:5);
        xlabel('s_0, m');
        ylabel('A_y, m/s^2');
    subplot(2,2,4);
        plot(s0,psi,'LineWidth',2);
        grid on;
        %set(gca, 'YTick',-5:1:10);
        xlabel('s_0, m');
        ylabel('psi_0, grad');
saveas(fig,'grt3.png');
saveas(fig,'grt3.fig');