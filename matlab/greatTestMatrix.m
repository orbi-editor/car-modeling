%great test 2
clear all;
addPath('rkf block');
addPath('matrix control block');
clc;

vs0=[20,0,0,0,0,0,0,0];%��������� ������ ��������
vsEnd=[20,0,0,0,60,3,0,60];%�������� �������� ������ ���������
vsTemp=vs0;%������� ������ ���������
u0=0;%��������� ����������. �������������� �� ����� ������
uEnd=0;%�������� ����������. �������������� �� ����� ������
dT=0.001;%�������� �������������� ������� ������ � ����� ��������
L=60;%����� ������ (���� �� ����)

T=0.01;%�������� ������������� ��� ��� ����� ��������� ����������

ttemp=0;%������������� �������
i=1;
hOld=0;
while ttemp<3.1
    tic;
    [control(i),h(i)]=matrixController(hOld,vs0,vsTemp,vsEnd,u0,uEnd,dT,L);%������� ����������
    toc;%����� ������� ���������� ������� ����������
    x0(i)=vsTemp(5);
    %x0(end)
    y0(i)=vsTemp(6);
    s0(i)=vsTemp(8);
    %s0(end)
    vsTemp=myCar(vsTemp,control(i),ttemp,T);%�������� ��������� �/�
    %vsTemp=filter1(vsTemp);%������ ��� ���������� ������
    ttemp=ttemp+T;
    hOld=h(i);
    i=i+1;
end

control=control./pi.*180;
h=h./pi.*180;

fig=figure(1);
    subplot(2,1,1);
        plot(x0,y0);
        grid on;
        xlabel('x0, m');
        ylabel('y0, m');
    subplot(2,1,2);
        plot(s0,h,s0,control);
        grid on;
        xlabel('s0, m');
        ylabel('h, control, grad');
saveas(fig,'greatTestMatrix.png');
saveas(fig,'greatTestMatrix.fig');