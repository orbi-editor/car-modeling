%���������� ����������� ����������� �� h
%��� ��������� ����� �������

clear all;
addPath('simple control block');
clc;

vs0=[20,0,0,0,0,0,0,0];%��������� ������ ��������
vsEnd=[20,0,0,0,60,3,0,60];%�������� �������� ������ ���������
vsTemp=vs0;%������� ������ ���������
u0=0;%��������� ����������. �������������� �� ����� ������
uEnd=0;%�������� ����������. �������������� �� ����� ������
dT=0.01;%�������� �������������� ������� ������ � ����� ��������
L=60;%����� ������ (���� �� ����)

h=0:0.05:40*pi/180;

for i=1:length(h)
    [e_y0(i),e_psi(i)]=lightEPCost([h(i);h(i);0],vsTemp,vsEnd,u0,uEnd,dT,L);
end

fig=figure(1);
    subplot(2,1,1);
        plot(h*180/pi,e_y0);
        grid on;
        xlabel('h, grad');
        ylabel('delta(y_0), m');
    subplot(2,1,2);
        plot(h*180/pi,e_psi*180/pi);
        grid on;
        xlabel('h, grad');
        ylabel('delta(psi_0), grad');
saveas(fig,'fun_h.png');
saveas(fig,'fun_h.fig');