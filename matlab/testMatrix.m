clear all;
addPath('rkf block');
addPath('matrix control block');
clc;

%=====initial=====
vs0=[20,0,0,0,0,0,0,0];%��������� ������ �������� - ������
u0=0;%��������� ����������. �������������� �� ����� ������
uEnd=0;%�������� ����������. �������������� �� ����� ������

dT=0.001;%�������� �������������� ������� ������
L=60;%����� ������ (���� �� ����)
varVec=[0.3;0.3;0];

[vecx,vecu]=matrixPrepare(varVec,L,u0,uEnd);

%=====methods: varMatrix=====
clear vs vsTemp t;

vsTemp=vs0;
vsOld=vs0;%star
arrVs=vsTemp;

tTemp=0;
arrT=0;

B=getMatrixB(tTemp,vsTemp);
A= getMatrixA(tTemp,vsTemp);

%uTemp=matrixUprav(vsTemp(5),vecx,vecu);
uOld=matrixUprav(vsTemp(5),vecx,vecu);

dotVsOld=(A*vsOld')'+uOld*B;
%dotVs_=[0,0,0,0,0,0,0];

while vsTemp(5)<=L
    vsTemp=vsOld+dT*dotVsOld;
    tTemp=tTemp+dT;
    
    uTemp=matrixUprav(vsTemp(8),vecx,vecu);
    A=getMatrixA(tTemp,vsTemp);
    
    dotVsTemp=matrixBicycleModelAuto(vsOld,uOld,vsTemp,uTemp,dotVsOld,A,B);
    
    arrVs=[arrVs;vsTemp];%��������� ������ �����. ������������� ������
    arrT=[arrT;tTemp];
    
    vsOld=vsTemp;
    uOld=uTemp;
    dotVsOld=dotVsTemp;
end

t_A=arrT;
Vx_A=arrVs(:,1);
Vy_A=arrVs(:,2);
r_A=arrVs(:,3);
psi_A=arrVs(:,4);
x0_A=arrVs(:,5);
y0_A=arrVs(:,6);
delta_A=arrVs(:,7);
s0_A=arrVs(:,8);

l1=length(t_A)
for i=1:l1-1
    dt_A(i)=t_A(i+1)-t_A(i);
    dVx_A(i)=(Vx_A(i+1)-Vx_A(i))/dt_A(i);
    dVy_A(i)=(Vy_A(i+1)-Vy_A(i))/dt_A(i);
    dr_A(i)=(r_A(i+1)-r_A(i))/dt_A(i);
    dpsi_A(i)=(psi_A(i+1)-psi_A(i))/dt_A(i);
    dx0_A(i)=(x0_A(i+1)-x0_A(i))/dt_A(i);
    dy0_A(i)=(y0_A(i+1)-y0_A(i))/dt_A(i);
    ddelta_A(i)=(delta_A(i+1)-delta_A(i))/dt_A(i);
    ds0_A(i)=(s0_A(i+1)-s0_A(i))/dt_A(i);
end

    dt_A(l1)=dt_A(l1-1);
    dVx_A(l1)=dVx_A(l1-1);
    dVy_A(l1)=dVy_A(l1-1);
    dr_A(l1)=dr_A(l1-1);
    dpsi_A(l1)=dpsi_A(l1-1);
    dx0_A(l1)=dx0_A(l1-1);
    dy0_A(l1)=dy0_A(l1-1);
    ddelta_A(l1)=ddelta_A(l1-1);
    ds0_A(l1)=ds0_A(l1-1);

%=====methods: standart=====
clear vs vsTemp t;
vsTemp=vs0;
vs=vs0;
tTemp=0;
t=0;

while vsTemp(5)<=L
    u=matrixUprav(vsTemp(8),vecx,vecu);
    vsTemp=vsTemp+dT*bicycleModelAuto3(tTemp,vsTemp,u)';%������
    vs=[vs;vsTemp];%��������� ������ �����. ������������� ������
    t=[t;tTemp];
    tTemp=tTemp+dT;
end
% 
t_st=t;
Vx_st=vs(:,1);
Vy_st=vs(:,2);
r_st=vs(:,3);
psi_st=vs(:,4);
x0_st=vs(:,5);
y0_st=vs(:,6);
delta_st=vs(:,7);
s0_st=vs(:,8);

l1=length(t_st)
for i=1:l1-1
    dt_st(i)=t_st(i+1)-t_st(i);
    dVx_st(i)=(Vx_st(i+1)-Vx_st(i))/dt_st(i);
    dVy_st(i)=(Vy_st(i+1)-Vy_st(i))/dt_st(i);
    dr_st(i)=(r_st(i+1)-r_st(i))/dt_st(i);
    dpsi_st(i)=(psi_st(i+1)-psi_st(i))/dt_st(i);
    dx0_st(i)=(x0_st(i+1)-x0_st(i))/dt_st(i);
    dy0_st(i)=(y0_st(i+1)-y0_st(i))/dt_st(i);
    ddelta_st(i)=(delta_st(i+1)-delta_st(i))/dt_st(i);
    ds0_st(i)=(s0_st(i+1)-s0_st(i))/dt_st(i);
end

    dt_st(l1)=dt_st(l1-1);
    dVx_st(l1)=dVx_st(l1-1);
    dVy_st(l1)=dVy_st(l1-1);
    dr_st(l1)=dr_st(l1-1);
    dpsi_st(l1)=dpsi_st(l1-1);
    dx0_st(l1)=dx0_st(l1-1);
    dy0_st(l1)=dy0_st(l1-1);
    ddelta_st(l1)=ddelta_st(l1-1);
    ds0_st(l1)=ds0_st(l1-1);
    
%������ ������
si=0:L/2000:L;
%�� �����������:
y0i_st=interp1(s0_st,y0_st,si);
y0i_A=interp1(s0_A,y0_A,si);
delta_y0i=y0i_st-y0i_A;

psii_st=interp1(s0_st,psi_st,si);
psii_A=interp1(s0_A,psi_A,si);
delta_psii=(psii_st-psii_A);

ri_st=interp1(s0_st,r_st,si);
ri_A=interp1(s0_A,r_A,si);
delta_ri=ri_st-ri_A;

%=====plot=====
fig1=figure(1);
    subplot(2,2,1);
        plot(s0_st,y0_st,'-',s0_A,y0_A,'--');
        grid on;
        xlabel('s0');
        ylabel('y0');
        legend('������������� ������','��������������� ������',0);
        xlim([0 L]);
    subplot(2,2,2);
        plot(s0_st,psi_st*180/pi,'-',s0_A,psi_A*180/pi,'--');
        grid on;
        xlabel('s0');
        ylabel('psi');
        legend('������������� ������','��������������� ������',0);
        xlim([0 L]);
    subplot(2,2,3);
        plot(si,delta_y0i);
        grid on;
        xlabel('s0');
        ylabel('������ y0');
        xlim([0 L]);
        %legend('si,delta_ri',0);
    subplot(2,2,4);
        plot(si,delta_psii*180/pi);
        grid on;
        xlabel('s0');
        ylabel('������ psi');
        xlim([0 L]);
        %legend('�',0);
saveas(fig1,'testMatrix11.png');
saveas(fig1,'testMatrix11.fig');

fig2=figure(2);
    subplot(2,2,1);
        plot(s0_st,delta_st*180/pi,'-',s0_A,delta_A*180/pi,'--');
        grid on;
        xlabel('s0');
        ylabel('delta');
        legend('������������� ������','��������������� ������',0);
        xlim([0 L]);
    subplot(2,2,2);
        plot(s0_st,Vy_st,'-',s0_A,Vy_A,'--');
        grid on;
        xlabel('s0');
        ylabel('Vy');
        legend('������������� ������','��������������� ������',0);
        xlim([0 L]);
    subplot(2,2,3);
        plot(s0_st,r_st*180/pi,'-',s0_A,r_A*180/pi,'--');
        grid on;
        xlabel('s0');
        ylabel('r');
        xlim([0 L]);
        legend('������������� ������','��������������� ������',0);
    subplot(2,2,4);
        plot(s0_st,Vx_st,'-',s0_A,Vx_A,'--');
        grid on;
        xlabel('s0');
        ylabel('Vx');
        xlim([0 L]);
        legend('������������� ������','��������������� ������',0);
saveas(fig2,'testMatrix21.png');
saveas(fig2,'testMatrix21.fig');