%plot cost(varVec)
clear all;
clc;
%test vector
vs0=[20,0,0,0,0,0,0,0];
vsEnd=[20,0,0,0,60,3,0,60];
dT=0.001;
n=100;
%u1=zeros(1,n);
%u2=zeros(1,n);
a=10;
b=26;
for i=1:n
    Ugrad(i)=a+(i-1)*(b-a)/n;
    Urad=Ugrad(i)*pi/180;
    [e_y0(i),e_psi(i)]=lightEPCost([Urad;Urad;0],vs0,vsEnd,dT);
end

fig=figure(1); 
    subplot(2,1,1);
        plot(Ugrad,e_y0);
        grid on;
        xlabel('upr, grad');
        ylabel('e_y0');
    subplot(2,1,2);
        plot(Ugrad,e_psi);
        grid on;
        xlabel('upr, grad');
        ylabel('e_psi');
saveas(fig,'myCost.png');
saveas(fig,'myCost.fig');
