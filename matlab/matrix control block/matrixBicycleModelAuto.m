function dotVs_=matrixBicycleModelAuto(vs,u,vs_,u_,dotVs,A,B)
%������� ���������������� ��������� ��� ���������
% x_ <=> x* - ������� �����
deltaVs=vs_-vs;
deltaU=u_-u;
deltaDotVs=(A*deltaVs')'+deltaU*B;
dotVs_=deltaDotVs+dotVs;
end