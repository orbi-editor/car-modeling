clear all;
clc;
varVec=[0.303;0.303];%test vector
vs0=[20,0,0,0,0,0,0,0];
vsEnd=[20,0,0,0,60,3,0,60];
n=30;
for i=1:n
    st=(i/10+1.7);
    dT(i)=0.1^st;
    %tic;
    res(i)=lightEPCost(varVec,vs0,vsEnd,dT(i));
    %t(i)=toc;
    lo(i)=st;
end

plot(lo,res,'-o'); grid on;
%plot(x0,y0); grid on; hold on;