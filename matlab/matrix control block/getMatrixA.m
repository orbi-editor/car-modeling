function A= getMatrixA(t,vec)
%   GETMATRIX A=dF/dX
%   vec=x*
%const
m=2000;
a=1.2;
b=1.8;
%Ksr=16;
I2=1800;
tau=0.008;
Cf=100000;
Cr=130000;
%Cf=10;
%Cr=13;

%state vector: Vx,Vy,r,psi,x0,y0,delta
Vx=vec(1);
Vy=vec(2);
r=vec(3);
psi=vec(4);
x0=vec(5);
y0=vec(6);

delta=vec(7);
s0=vec(8);

k1=delta-(Vy+a*r)/Vx;

%(Vy-b*r)/Vx%/atan((Vy-b*r)/Vx)

A(1,1)=-Cr*sin(delta)*(Vy+a*r)/m/Vx^2;
A(1,2)=Cf*sin(delta)/m/Vx+r;
A(1,3)=Cf*a*sin(delta)/m/Vx+Vy;
A(1,4)=0;
A(1,5)=0;
A(1,6)=0;
A(1,7)=-Cf*(k1*cos(delta)+sin(delta))/m;
A(1,8)=0;

A(2,1)=-r-(Cr*(b*r-Vy)-Cf*(Vy+a*r)*cos(delta))/m/Vx^2;
A(2,2)=-(Cr+Cf*cos(delta))/m/Vx;
A(2,3)=(Cr*b-Cf*a*cos(delta))/m/Vx-Vx;
A(2,4)=0;
A(2,5)=0;
A(2,6)=0;
A(2,7)=Cf*(cos(delta)-k1*sin(delta))/m;
A(2,8)=0;

A(3,1)=-(Cr*b*(Vy-b*r)-Cf*a*(Vy+a*r)*cos(delta))/I2/Vx^2;
A(3,2)=(b*Cr-a*Cf*cos(delta))/Vx/I2;
A(3,3)=-(b*Cr*b-a*Cf*a*cos(delta))/Vx/I2;
A(3,4)=0;
A(3,5)=0;
A(3,6)=0;
A(3,7)=a*Cf*(cos(delta)-k1*sin(delta))/I2;
A(3,8)=0;

A(4,1)=0;
A(4,2)=0;
A(4,3)=1;
A(4,5)=0;
A(4,6)=0;
A(4,7)=0;
A(4,8)=0;

A(5,1)=cos(psi);
A(5,2)=-sin(psi);
A(5,3)=0;
A(5,4)=-Vx*sin(psi)-Vy*cos(psi);
A(5,5)=0;
A(5,6)=0;
A(5,7)=0;
A(5,8)=0;

A(6,1)=sin(psi);
A(6,2)=cos(psi);
A(6,3)=0;
A(6,4)=Vx*cos(psi)-Vy*sin(psi);
A(6,5)=0;
A(6,6)=0;
A(6,7)=0;
A(6,8)=0;

A(7,1)=0;
A(7,2)=0;
A(7,3)=0;
A(7,4)=0;
A(7,5)=0;
A(7,6)=0;
A(7,7)=-1/tau;
A(7,8)=0;

A(8,1)=Vx/sqrt(Vx^2+Vy^2);
A(8,2)=Vy/sqrt(Vx^2+Vy^2);
A(8,3)=0;
A(8,4)=0;
A(8,5)=0;
A(8,6)=0;
A(8,7)=0;
A(8,8)=0;
end