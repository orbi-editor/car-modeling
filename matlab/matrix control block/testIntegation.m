clear all;
clc;
vecx=[0;10;40;50;60;90;100];
vecu=[0;0.3;0.3;0;-0.3;-0.3;0];
x0=[20;0;0;0;0;0;0;0];
t0=0;
tf=5;
dT=0.001;
[t,x]=mySolver(@(tt,xx)bicycleModelAuto2(tt,xx,vecx,vecu),x0,t0,tf,dT);